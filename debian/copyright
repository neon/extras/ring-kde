Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ring-kde
Source: https://download.kde.org/stable/ring-kde/
Files-Excluded-ring: client*
  docker
  docs
  lrc
  packaging
  scripts
  Makefile*
  make-ring.py
  CONTRIBUTING.md
  COPYING
  README.rst
  clean-DATA-LOSS.sh
  .cqfd
  daemon/MSVC
  daemon/contrib
  daemon/doc
  daemon/docker
  daemon/extras
  daemon/m4
  daemon/man
  daemon/ringtones
  daemon/test
  daemon/tools
  daemon/AUTHORS
  daemon/CODING
  daemon/COPYING
  daemon/ChangeLog
  daemon/Makefile.am
  daemon/NEWS
  daemon/README
  daemon/astylerc
  daemon/autogen.sh
  daemon/configure.ac
  daemon/globals.mk
  daemon/bin/jni
  daemon/bin/nodejs
  daemon/bin/restcpp
  daemon/bin/Makefile.am
  daemon/bin/*.cpp
  daemon/src/client
  daemon/src/config
  daemon/src/hooks
  daemon/src/im
  daemon/src/media
  daemon/src/ringdht
  daemon/src/security
  daemon/src/sip
  daemon/src/upnp
  daemon/src/*.cpp
  daemon/src/*.c
  daemon/src/Makefile.am
  daemon/src/account.h
  daemon/src/base64.h
  daemon/src/conference.h
  daemon/src/fileutils.h
  daemon/src/ip_utils.h
  daemon/src/peer_connection.h
  daemon/src/rw_mutex.h
  daemon/src/turn_transport.h
  daemon/src/account_factory.h
  daemon/src/call.h
  daemon/src/data_transfer.h
  daemon/src/ftp_server.h
  daemon/src/logger.h
  daemon/src/preferences.h
  daemon/src/smartools.h
  daemon/src/utf8_utils.h
  daemon/src/account_schema.h
  daemon/src/call_factory.h
  daemon/src/dlfcn.h
  daemon/src/generic_io.h
  daemon/src/manager.h
  daemon/src/rational.h
  daemon/src/string_utils.h
  daemon/src/windirent.h
  daemon/src/archiver.h
  daemon/src/channel.h
  daemon/src/ice_socket.h
  daemon/src/map_utils.h
  daemon/src/registration_states.h
  daemon/src/thread_pool.h
  daemon/src/winsyslog.h
  daemon/src/array_size.h
  daemon/src/compiler_intrinsics.h
  daemon/src/enumclass_utils.h
  daemon/src/ice_transport.h
  daemon/src/noncopyable.h
  daemon/src/ring_types.h
  daemon/src/threadloop.h
Comment: These files are not used in the building of ring-kde.

Files: *
Copyright: 2017, 2018 BlueSystems GmbH
           2015-2017 Emmanuel Lepage Vallee
           2009-2018, Savoir-faire Linux
License: LGPL-2.1+

Files: man/*
Copyright: This program is free software; you can redistribute it and/or modify it
License: GPL-3

Files: src/accountview/*
       src/callview/*
       src/*
       src/canvasindicators/*
       src/conf/accountpages/*
       src/conf/accountserializationadapter.cpp
       src/conf/accountserializationadapter.h
       src/conf/dlgaccount.cpp
       src/conf/dlgaccount.h
       src/configurator/*
       src/configurator/fallbackpersonconfigurator.cpp
       src/configurator/fallbackpersonconfigurator.h
       src/configurator/localhistoryconfigurator.cpp
       src/configurator/localhistoryconfigurator.h
       src/contactview/*
       src/delegates/righticondelegate.cpp
       src/delegates/righticondelegate.h
       src/desktopview/*
       src/dialview/*
       src/notification.cpp
       src/notification.h
       src/photoselector/*
       src/qmlwidgets/*
       src/timeline/*
       src/widgets/directoryview.cpp
       src/widgets/directoryview.h
       src/widgets/menumodelview.cpp
       src/widgets/menumodelview.h
       src/wizard/*
       templates/*
       libringqt/cmake/*
       data/*
       debian/*
       ring/*
Copyright: 2017, 2018 Bluesystems
           2004-2018 Savoir-Faire Linux
           2015 Emmanuel Lepage Vallee
           2016 Marat Moustafine
           2018 Scarlett Moore <sgclark@kde.org>
License: GPL-3+

Files: src/widgets/categorizedtreeview.cpp
Copyright: 2012, Savoir-Faire Linux
License: LGPL-2

Files: src/widgets/groupedtoolbutton.cpp
  src/widgets/groupedtoolbutton.h
Copyright: 2007, Aurélien Gâteau <agateau@kde.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991, or (at
 your option) any later version.
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-2'.

License: GPL-3
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 dated June, 2007.
 On Debian systems, the complete text of version 3 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-3'.

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 dated June, 2007, or (at
 your option) any later version.
 On Debian systems, the complete text of version 3 of the GNU General
 Public License can be found in '/usr/share/common-licenses/GPL-3'.

License: LGPL-2
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU Library General Public License as published by the
 Free Software Foundation; version 2 of the License.
 On Debian systems, the complete text of version 2 of the GNU Library
 General Public License can be found in '/usr/share/common-licenses/LGPL-2'.

License: LGPL-2.1+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License as published by the
 Free Software Foundation; version 2.1 of the License, or (at
 your option) any later version.
 On Debian systems, the complete text of version 2.1 of the GNU Lesser
 General Public License can be found in '/usr/share/common-licenses/LGPL-2.1'.
